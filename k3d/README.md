
# K3D Kubernetes

Today we will learn:

- What is K3D
- How to install on your local machine
- How to use the registry locally
- Some basics about Kubernetes
- How to run your application on a multi-cluster Kubernetes locally
- How to administer the K3D


```bash
    Ingress -> Service -> Deployment -> Pod -> Containers
```


## Demo

- How to build a cluster using CLI and config file
- How to push image to registry
- Traefik Dashboard
